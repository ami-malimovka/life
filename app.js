/** app entry point,  manages windows and calls the simulation logic */

const path = require('path');
const url = require('url');
const electron = require('electron');
const {app, BrowserWindow, ipcMain} = electron;

/******** window management *******/
let mainWindow;

function createWindow () {
    mainWindow = new BrowserWindow({width: 800, height: 600});
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'frontend', 'index.html'),
        protocol: 'file:',
        slashes: true,
    }));

    mainWindow.on('closed', function () {
        mainWindow = null;
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
    if (mainWindow === null) {
        createWindow();
    }
});
/******** end of window management *******/

/******** game backend ***********/
const PatternStore = require('./patterns/patternStore');
const Driver = require('./driver');

ipcMain.on('gui:ready', () => {
    let patternStore = new PatternStore();
    let driver = new Driver(ipcMain, mainWindow, patternStore);
    driver.init();
});

/******** end of game backend ***********/
