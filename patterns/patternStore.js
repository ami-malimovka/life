const fs = require('fs');

const PATTERNS_DIR = './patterns';

module.exports = class PatternStore {

    constructor () {
        this.patterns = [];
    }

    getMinimalList () {
        let res = [];
        for (let i = 0; i < this.patterns.length; i++) {
            res.push({'id': i, 'name': this.patterns[i].name});
        }
        return res;
    }

    buildList () {
        return new Promise((resolve, reject) => {
            fs.readdir(PATTERNS_DIR, function (err, filenames) {
                if (err) {
                    reject(err);
                    return;
                }

                filenames = filenames.filter(fname => /\.json$/.test(fname));

                let patterns = [];
                let filesToProcessCount = filenames.length;
                filenames.forEach(function (filename) {
                    fs.readFile(PATTERNS_DIR + '/' + filename, 'utf-8', function (err, content) {
                        if (err) {
                            reject(err);
                            return;
                        }
                        try {
                            let patternData = JSON.parse(content);
                            patterns.push(patternData);
                        } catch (e) {
                            reject(e);
                        }
                        filesToProcessCount--;
                        if (0 === filesToProcessCount) {
                            resolve(patterns);
                        }
                    });
                });
            });
        });

    }

    add () {

    }

    remove () {

    }

    find (id) {
        return this.patterns[id];
    }

};