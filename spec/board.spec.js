/**
 * Created by ami on 11/20/17.
 */

const Board = require('../backend/board');

describe('Board tests', () => {

    const ROWS = 10;
    const COLS = 10;
    let board;

    beforeEach(function () {
        board = new Board(ROWS, COLS);
    });

    it('starts with a DEAD board', () => {
        for (let i = 0; i < ROWS; i++) {
            for (let j = 0; j < COLS; j++) {
                expect(board.getCell(i, j)).toEqual(Board.DEAD);
            }
        }
    });

    it('is a toroidal board', () => {
        expect(board.getCell(0, 0)).toEqual(Board.DEAD);
        expect(board.getCell(ROWS, COLS)).toEqual(Board.DEAD);
        board.setCell(0, 0, Board.ALIVE);
        expect(board.getCell(0, 0)).toEqual(Board.ALIVE);
        expect(board.getCell(ROWS, COLS)).toEqual(Board.ALIVE);
    });

    it('calculates live friends', () => {
        board.setCell(0, 0, Board.ALIVE);
        expect(board.countLiveFriends(0, 0)).toEqual(0);
        expect(board.countLiveFriends(0, 1)).toEqual(1);
    });

    it('can have a new and shiny grid assigned to it', () => {
        board.setGrid([
            [Board.DEAD, Board.ALIVE],
            [Board.DEAD, Board.ALIVE],
            [Board.DEAD, Board.ALIVE],
        ]);

        expect(board.getRows()).toEqual(3);
        expect(board.getCols()).toEqual(2);

        expect(() => {board.setGrid(1);}).toThrow('expected grid to be an array');
        expect(() => {board.setGrid([]);}).toThrow('expected grid to be a non empty array');
        expect(() => {board.setGrid([1]);}).toThrow('expected grid to be a two dimensional array');
    });

});