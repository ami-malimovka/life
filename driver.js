/**
 * connects the gui and the backend, responsible for managing the Game instance
 */
const Game = require('./backend/game');

const TICK_DELAY_UNIT = 0.5 * 1000;
const MAX_SPEED = -4;
const MIN_SPEED = 4;

module.exports = class Driver {
    constructor (ipcMain, mainWindow, patternStore) {
        this.ipcMain = ipcMain;
        this.mainWindow = mainWindow;
        this.patternStore = patternStore;
        this.theGame = new Game();
        this.gameState = 'stopped';
        this.cycle = 1;
        this.tickSpeed = 0;
        this.timer = null;
    };

    init () {
        this.hookEvents();
        this.sendCycleDataToGui();
        this.startTicking();
        this.sendPatternsData();
    }

    hookEvents () {

        this.ipcMain.on('game:startStop', (e, data) => {
            this.onStartStop(data);
        });

        this.ipcMain.on('patterns:load', (e, data) => {
            this.loadPattern(data);
        });

        this.ipcMain.on('game:changeSpeed', (e, data) => {
            this.onChangeSpeed(data);
        });
    }

    onStartStop (data) {
        if (data.gameState) {
            this.gameState = data.gameState;
        } else {
            this.flipGameState();
        }

        if (data.reset === true) {
            this.cycle = 0;
        }

        this.theGame.setGrid(data.grid);
        this.sendCycleDataToGui();
    }

    flipGameState () {
        this.gameState = ('stopped' === this.gameState)
            ? 'started'
            : 'stopped';
    }

    sendCycleDataToGui () {
        this.mainWindow.webContents.send('cycle:new', {
            'grid': this.theGame.getGrid(),
            'cycle': this.cycle,
            'gameState': this.gameState,
        });
        this.mainWindow.webContents.send('game:' + this.gameState);
    }

    startTicking () {
        this.timer = setInterval(() => {
            if ('started' === this.gameState) {
                this.theGame.nextCycle();
                this.cycle++;
                this.sendCycleDataToGui();
            }
        }, TICK_DELAY_UNIT + this.tickSpeed * 100);
    }

    onChangeSpeed (data) {
        if (data.change) {
            if (data.change === 'faster') {
                this.tickSpeed = Math.max(MAX_SPEED, this.tickSpeed - 1);
            } else {
                this.tickSpeed = Math.min(MIN_SPEED, this.tickSpeed + 1);
            }
            clearInterval(this.timer);
            this.timer = null;
            this.startTicking();
        }
    }

    sendPatternsData () {
        this.patternStore.buildList().then((data) => {
            this.patternStore.patterns = data;
            this.mainWindow.webContents.send('patterns:data', {
                'list': this.patternStore.getMinimalList(),
            });
        });
    }

    loadPattern (data) {
        let pattern = this.patternStore.find(data.id);
        this.gameState = 'stopped';
        this.cycle = 0;
        this.theGame.setGrid(pattern.grid);
        this.sendCycleDataToGui();
    }
};