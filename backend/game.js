/** holds the simulation rules, is used to calculate the next cycle.  */

const Board = require('./board');

const DEFAULT_BOARD_SIZE = 10;

module.exports = class Game {
    constructor () {
        this.board = new Board(DEFAULT_BOARD_SIZE, DEFAULT_BOARD_SIZE);
    }

    setGrid (grid) {
        this.board.setGrid(grid);
    }

    getGrid () {
        return this.board.getGrid();
    }

    setBoard (board) {
        this.board = board;
        return this;
    }

    getBoard () {
        return this.board;
    }

    /**
     *  sets this.board to the next simulation step
     *
     * - two neighbours preserve state
     * - three neighbours create life
     * - more than three cause death by overcrowding, less than two cause
     *    death by loneliness
     * */
    nextCycle () {
        let rows = this.board.getRows();
        let cols = this.board.getCols();
        let newBoard = new Board(rows, cols);

        //TODO: this can be more efficient (many redundant calculations)
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < cols; j++) {
                let liveCount = this.board.countLiveFriends(i, j);
                if (2 === liveCount) {
                    newBoard.setCell(i, j, this.board.getCell(i, j));
                } else if (3 === liveCount) {
                    newBoard.setCell(i, j, Board.ALIVE);
                } else {
                    newBoard.setCell(i, j, Board.DEAD);
                }
            }
        }
        this.board = null;
        this.board = newBoard;
    }
};