/** represents a toroidal grid */

const DEAD = 0;
const ALIVE = 1;

module.exports = class Board {
    constructor (rows, cols) {
        this.rows = rows;
        this.cols = cols;
        this.grid = [];

        this.initGrid();
    }

    static get DEAD () {
        return DEAD;
    }

    static get ALIVE () {
        return ALIVE;
    }

    initGrid () {
        for (let i = 0; i < this.rows; i++) {
            this.grid[i] = [];
            for (let j = 0; j < this.cols; j++) {
                this.grid[i][j] = DEAD;
            }
        }
    }

    getCell (row, col) {
        return this.grid[this.myMod(row, this.rows)][this.myMod(col, this.cols)];
    }

    /** returns non negative n % N  */
    myMod (n, N) {
        if (n >= 0) {
            return n % N;
        }
        while (n < 0) {
            n += N;
        }
        return n % N;
    }

    setCell (row, col, val) {
        this.grid[this.myMod(row, this.rows)][this.myMod(col, this.cols)] = val;
        return this.getCell(row, col);
    }

    countLiveFriends (row, col) {
        let res = 0;
        for (let i = -1; i < 2; i++) {
            for (let j = -1; j < 2; j++) {
                res += this.getCell(row + i, col + j);
            }
        }
        res -= this.getCell(row, col);
        return res;
    }

    setGrid (grid) {
        if (!(Array.isArray(grid))) {
            throw 'expected grid to be an array';
        }
        if (0 === grid.length) {
            throw 'expected grid to be a non empty array';
        }
        if (!(Array.isArray(grid[0]))) {
            throw 'expected grid to be a two dimensional array';
        }
        this.grid = null; // probably unnecessary, but paranoia is good.
        this.grid = grid;
        this.rows = grid.length;
        this.cols = grid[0].length;
        return this;
    }

    getGrid () {
        return this.grid;
    }

    getCols () {
        return this.cols;
    }

    getRows () {
        return this.rows;
    }

};